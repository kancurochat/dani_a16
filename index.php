<?php 

    // Importamos el archivo bootstrap
    require "core/bootstrap.php";


    // Almacenamos el array de rutas en $routes
    $routes = require "app/routes.php";

    // Importamos la ruta que sea accedida en cada petición GET
    require $routes[Request::uri()];

?>