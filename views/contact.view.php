<?php 

	include __DIR__ . "/partials/inicio-doc.part.php";

	$errors = [];
	$fail = false;
	$data = [];

	$fname = '';
	$lname = '';
	$email = '';
	$subject = '';
	$msg = '';

	if (isset($_POST['send'])) {
		$fname = htmlspecialchars(trim($_POST['fname']));
		$lname = htmlspecialchars(trim($_POST['lname']));
		$email = htmlspecialchars(trim($_POST['email']));
		$subject = htmlspecialchars(trim($_POST['subject']));
		$msg = htmlspecialchars(trim($_POST['msg']));
		if ($fname == '' || $email == '' || $subject == '') {
			$errors[] ='First name, email and subject are required';
			$fail = true;
		}else {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$errors[] = 'Insert a valid email';
				$fail = true;
			}
		}
	}

	if ($fail) {
		$content = implode('\n', $errors);
		$div = "<div class=\"col-xs-12 col-sm-8 col-sm-push-2 h3\">$content</div>";
	}else {
		$data[] = $fname;
		$data[] = $lname;
		$data[] = $email;
		$data[] = $subject;
		$data[] = $msg;
		$content = implode(' ', $data);
		$div = "<div class=\"col-xs-12 col-sm-8 col-sm-push-2 h3 text-white\">$content</div>";
	}

?>

<?php include __DIR__ . "/partials/nav.part.php";?>

<!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACT US</h1>
       	   <hr>
       	   <p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
		   <?php if($fail) echo $div; ?>
			<form class="form-horizontal" method="post">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">First Name</label>
	       	  		<input class="form-control" type="text" name="fname" value="<?= $fname ?>">
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Last Name</label>
	       	  		<input class="form-control" type="text" name="lname" value="<?= $lname ?>">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Email</label>
	       	  		<input class="form-control" type="text" name="email" value="<?= $email ?>">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Subject</label>
	       	  		<input class="form-control" type="text" name="subject" value="<?= $subject ?>">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Message</label>
	       	  		<textarea class="form-control" name="msg"><?= $msg ?></textarea>
	       	  		<button class="pull-right btn btn-lg sr-button" name="send">SEND</button>
	       	  	</div>
	       	  </div>
		   </form>
		   <hr class="divider">
		   <?php if(!$fail) { 
			   	echo "<br>";
				echo $div;
				echo "<br>";
			}?>
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<?php include __DIR__ . "/partials/fin-doc.part.php" ?>