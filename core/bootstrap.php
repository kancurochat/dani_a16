<?php 

session_start();

require_once "App.php";
require_once "Request.php";
require_once "vendor/autoload.php";
require_once "utils/MyLog.php";

$config = require_once __DIR__ . "/../app/config.php";
$logger = MyLog::load('logs/info.log');

App::bind("config", $config);

App::bind('logger', $logger); 

?>