<?php 

include __DIR__ . "../../exceptions/QueryException.php";
require_once __DIR__ . "../../exceptions/NotFoundException.php";
require_once __DIR__ . "../../core/App.php";

abstract class QueryBuilder {

    private $connection;

    private $table;

    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    public function executeQuery(string $sql) {

        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute() === false) {

            throw new QueryException("No se ha podido ejecutar la consulta");

        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);

    }

    // Prepara una consulta de todos los registros de una tabla,
    // la ejecuta en la base de datos y los devuelve en un array de objetos
    public function findAll() {

        $sql = "SELECT * FROM $this->table";

        $result = $this->executeQuery($sql);

        if(empty($result)) {

            throw new QueryException("No se han encontrado registros");

        }

        return $result;

    }

    public function save(IEntity $entity): void {

        try {

            //Convierte la entidad a un array
            $parameters = $entity->toArray();

            


            // Prepara una consulta preparada usando como parámetros las 
            // claves del array convertido
            $sql = sprintf("INSERT INTO %s (%s) values (%s)",
        
                $this->table,

                implode(", ", array_keys($parameters)),

                ":". implode(", :", array_keys($parameters))

            );

            // Prepara la consulta
            $statement = $this->connection->prepare($sql);

            //Ejecuta la consulta
            $statement->execute($parameters);

            

            

        }catch(PDOException $exception) {

            throw new QueryException("Error al insertar en la BBDD");

        }

    }

    public function find(int $id): IEntity {

        $sql = "SELECT * FROM $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if(empty($result)) {

            throw new NotFoundException("No se ha encontrado el elemento con id $id");

        }

        return $result[0];

    }
    
}



?>