<?php 

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLog {

    private $log;

    private function __construct(string $filename) {

        $this->log = new Logger('DSW');

        $this->log->pushHandler(new StreamHandler($filename, Logger::INFO));

    }

    public static function load(string $filename) {

        return new MyLog($filename);

    }

    public function add(string $message) {

        $this->log->info($message);

    }

}


?>