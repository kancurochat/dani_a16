<?php 

include "exceptions/FileException.php";

class File {


    private $file;
    private $fileName;

    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if(($this->file["name"] == "")){

            throw new FileException("Debes especificar un fichero", 1);

        }

        if($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    throw new FileException("El fichero es demasiado pesado", 2);
                    break;

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("El fichero no pudo subirse por completo", 3);
                    break;

                default:
                    
                    throw new FileException("Hubo un error en la subida del fichero", 4);
                    break;


            }

        }

        if(in_array($this->file["type"], $arrTypes) === false) {

            throw new FileException("El fichero no tiene un formato adecuado", 5);

        }

    }

    /**
     * Get the value of fileName
     */ 
    public function getFileName()
    {
        return $this->file['name'];
    }

    public function saveUploadFile(string $ruta) {

        if (!is_uploaded_file($this->file['tmp_name'])) {
            
            throw new FileException("El archivo no se ha subido mediante el formulario", 6);

        }

        $rutaDestino = $ruta.$this->file['name'];

        if (is_file($rutaDestino)) {

            $idUnico = time();
            $this->file['name'] = $idUnico . $this->file['name'];
            $rutaDestino = $ruta.$this->file['name']; 

        }

        if(move_uploaded_file($this->file['tmp_name'], $rutaDestino) === false){
            
            throw new FileException("No se ha podido mover el fichero al destino especificado");

        };

    }

    public function copyFile ($rutaOrigen, $rutaFinal) {

        $ficheroOrigen = $rutaOrigen.$this->file['name'];
        $ficheroDestino = $rutaFinal.$this->file['name'];

        if (!is_file($ficheroOrigen)) {
            throw new FileException("No existe el fichero $ficheroOrigen");
        }

        if (is_file($rutaFinal)) {
            throw new FileException("El fichero $ficheroDestino");
        }

        if(copy($ficheroOrigen, $ficheroDestino) === false) {

            throw new FileException("No se ha podido copiar el fichero");

        }

    }

}
