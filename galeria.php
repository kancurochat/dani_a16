<?php

include __DIR__ . "/database/Connection.php";
include __DIR__ . "/database/QueryBuilder.php";
include __DIR__ . "/utils/utils.php";
require_once __DIR__ . "/entity/ImagenGaleria.php";
require_once __DIR__ . "/entity/Categoria.php";
include __DIR__ . "/utils/File.php";
require_once __DIR__ . "/core/App.php";
require_once __DIR__ . "/repository/ImagenGaleriaRepository.php";
require_once __DIR__ . "/repository/CategoriaRepository.php";

$mensaje = "";




try {

    $config = require_once __DIR__ . "/app/config.php";

    // Guardamos la configuración en el contenedor
    App::bind("config", $config);

    $categoriaRepository = new CategoriaRepository();

    $imagenGaleriaRepository = new ImagenGaleriaRepository();


    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $categoria = trim(htmlspecialchars($_POST['categoria']));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $rutaOrigen = "/var/www/html/" . "Dani_A16" . "/images/index/gallery/";

        $imagen->saveUploadFile($rutaOrigen);
        $imagen->copyFile($rutaOrigen, "/var/www/html/Dani_A16/images/index/portfolio/");

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $categoria, $descripcion);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Datos enviados";
    }

    $imagenes = $imagenGaleriaRepository->findAll();

    $categorias = $categoriaRepository->findAll();

} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch(AppException $appException) {

    $errores[] = $appException->getMessage();

}catch(QueryException $queryException) {

    $errores[] = $queryException->getMessage();

}



require "views/galeria.view.php";
