<?php 

require_once __DIR__ . "../../database/IEntity.php";


class ImagenGaleria implements IEntity {
    
    private $id;

    private $nombre;

    private $descripcion;

    private $numVisualizaciones;

    private $numLikes;

    private $numDownloads;

    private const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";

    public const RUTA_IMAGENES_GALERIA = "images/index/gallery/";

    private $categoria;

    public function __construct($id = "", $nombre = "", $categoria=0, $descripcion = "", $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0)
    {

        $this->id = $id;
        $this->nombre = $nombre;
        $this->categoria = $categoria;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
        
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get the value of numVisualizaciones
     */ 
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * Get the value of numLikes
     */ 
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Get the value of numDownloads
     */ 
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }

    public function getURLPortfolio() : string {

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

    }

    public function getURLGallery() : string {

        return self::RUTA_IMAGENES_GALERIA . $this->getNombre();

    }


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

     /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function toArray(): array
    {
        
        return [

            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "categoria" => $this->getCategoria(),

            "descripcion" => $this->getDescripcion(),

            "numVisualizaciones" => $this->getNumVisualizaciones(),

            "numLikes" => $this->getNumLikes(),

            "numDownloads" => $this->getNumDownloads()  

        ];

    }

   
}

?>