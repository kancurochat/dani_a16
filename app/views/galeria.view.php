<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php"; 

?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <form class="form-horizontal" action="<?php //$_SERVER["PHP_SELF"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                    <div class="col-xs-12">
                        <label for="categoria" class="label-control">Categoría</label>
                        <select name="categoria" class="form-control">

                        <?php foreach($categorias as $categoria) : ?>

                            <option value="<?= $categoria->getId() ?>" <?= $categoriaSeleccionada == $categoria->getId() ? 'selected' : '' ?> ><?= $categoria->getNombre()?></option>

                        <?php endforeach; ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"><?= $descripcion ?? "" ?></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<table class="table">
<?php foreach ($imagenes as $imagen) : ?>
            <tr>
                <th scope="row"><?= $imagen->getId() ?></th>
                <th scope="col">Categoría</th>
                <td><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre() ?></td>
                <td>
                    <img src="<?= $imagen->getURLGallery() ?>" alt="<?= $imagen->getDescripcion() ?>" title="<?= $imagen->getDescripcion() ?>" width="100px">
                </td>
                <td><?= $imagen->getNumVisualizaciones() ?></td>
                <td><?= $imagen->getNumLikes() ?></td>
                <td><?= $imagen->getNumDownloads() ?></td>
            </tr>
        <?php endforeach; ?>
</table>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>