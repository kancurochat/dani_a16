<!-- Navigation Bar -->
<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class="<?php if(isPage("index")) echo "active"; ?> lien"><a href="<?php if(isPage("index")) echo "#"; else echo "/Dani_A16"; ?>"><i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class="<?php if(isPage("about")) echo "active"; ?> lien"><a href="<?php if(isPage("about")) echo "#"; else echo "about"; ?>"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class="<?php if(isPage("blog")) echo "active"; ?> lien"><a href="<?php if(isPage("blog")) echo "#"; else echo "blog"; ?>"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class="<?php if(isPage("contact")) echo "active"; ?> lien"><a href="<?php if(isPage("contact")) echo "#"; else echo "contact"; ?>"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class="<?php if(isPage("galeria")) echo "active"; ?>"><a href="<?php if(isPage("galeria")) echo "#"; else echo "galeria"; ?>"><i class="fa fa-image sr-icons"></i> Gallery</a></li>
            </ul>
         </div>
     </div>
   </nav>
<!-- End of Navigation Bar -->