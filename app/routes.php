<?php 

    return [

        "Dani_A16" => "app/controllers/index.php",

        "Dani_A16/about" => "app/controllers/about.php",

        "Dani_A16/blog" => "app/controllers/blog.php",

        "Dani_A16/contact" => "app/controllers/contact.php",

        "Dani_A16/galeria" => "app/controllers/galeria.php",

        "Dani_A16/post" => "app/controllers/single_post.php"

    ];

?>