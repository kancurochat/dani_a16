<?php

include "database/QueryBuilder.php";
include "utils/utils.php";
require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";
include "utils/File.php";
require_once "core/App.php";
require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";
require_once "core/helpers/FlashMessage.php";


$mensaje = "";


try {

    $categoriaRepository = new CategoriaRepository();

    $imagenGaleriaRepository = new ImagenGaleriaRepository();

    $categoriaSeleccionada = '';

    $categorias = $categoriaRepository->findAll();

    $imagenes = $imagenGaleriaRepository->findAll();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        FlashMessage::set("descripcion", $descripcion);

        $categoriaSeleccionada = trim(htmlspecialchars($_POST['categoria']));

        FlashMessage::get("categoria", $categoriaSeleccionada);

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $rutaOrigen = "/var/www/html/" . "Dani_A16" . "/images/index/gallery/";

        $imagen->saveUploadFile($rutaOrigen);
        $imagen->copyFile($rutaOrigen, "/var/www/html/Dani_A16/images/index/portfolio/");

        $imagenGaleria = new ImagenGaleria(0, $imagen->getFileName(), $categoriaSeleccionada, $descripcion);

        $imagenGaleriaRepository->save($imagenGaleria);

        $_SESSION["mensajes"] = "Datos enviados";

        App::get("logger")->add($mensaje);
    }

    FlashMessage::unset("descripcion");
    FlashMessage::unset("categoria");

    $descripcion = "";
    $categoriaSeleccionada = "";
    

} catch (FileException $fileException) {

    FlashMessage::set("errores", [$fileException->getMessage()]);

} catch(AppException $appException) {

    FlashMessage::set("errores", [$appException->getMessage()]);

}catch(QueryException $queryException) {

    FlashMessage::set("errores", [$queryException->getMessage()]);

}

$errores = FlashMessage::get("errores");

unset($_SESSION["errores"]);

$mensaje = $_SESSION["mensajes"] ?? "";

unset($_SESSION["mensajes"]);

require "app/views/galeria.view.php";