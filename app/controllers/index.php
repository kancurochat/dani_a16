<?php
    
    include "utils/utils.php";
    require "entity/ImagenGaleria.php";
    require_once "repository/ImagenGaleriaRepository.php";

    try {

        /* $config = require_once __DIR__ . "/../config.php";
    
        // Guardamos la configuración en el contenedor
        App::bind("config", $config); */
    
        $imagenGaleriaRepository = new ImagenGaleriaRepository();
    
        $galeria = $imagenGaleriaRepository->findAll();
    
    
    } catch(AppException $appException) {
    
        $errores[] = $appException->getMessage();
    
    }catch(QueryException $queryException) {
    
        $errores[] = $queryException->getMessage();
    
    }

    require "app/views/index.view.php";
    
?>