<?php 

return [

    "database" => [

        "name" => "fotografias",

        "username" => "user_fotos",

        "password" => "server",

        "connection" => "mysql:host=localhost",

        "options" => [

            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,

            PDO::ATTR_PERSISTENT => true

        ]

    ]

];

?>